const express = require('express');
const mongoose = require('mongoose');
const User = require('./models/user.js');
const bodyParser = require('body-parser');
const config = require('../static/config.js');
const app = express()

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
mongoose.set('useFindAndModify', false);

async function start() {
    try {
        const url = `mongodb+srv://ivan:Ivan1234@cluster0-8yu3p.gcp.mongodb.net/test`;
        await mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true})
        app.listen(config.mongoServerPort, () => {
            console.log(`Mongo server is running on port ${config.mongoServerPort} ...`)
        })
    } catch (e) {
        console.log(e)
    }
}
start();

app.post('/', async (req, res) => {
    const user = new User({
        _id: req.body._id,
        login: req.body.login,
    });
    try {
        await user.save()
    } catch (e) {
        console.log(e)
    }
});

app.post('/add-session', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {activeSessions: 1},
        browser: req.body.browser,
        publicIP: req.body.publicIP,
        device: req.body.device,
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/add-geo', async (req, res) => {
    const id = parseInt(req.body._id);
    User.findOneAndUpdate({_id: id}, {
        geo: req.body.geo
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/students-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timeStudents: req.body.timeStudents},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/paint-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timePaint: req.body.timePaint},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/calc-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timeCalculator: req.body.timeCalculator},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/account-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timeAccount: req.body.timeAccount},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/currency-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timeCurrency: req.body.timeCurrency},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/converter-timer', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {timeConverter: req.body.timeConverter},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/click-create', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {createClick: 1},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/click-update', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {updateClick: 1},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/click-delete', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {deleteClick: 1},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/click-clear', async (req, res) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        '$inc': {clearClick: 1},
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.post('/click-message', async (req, res) => {
    var currentdate = new Date();
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
        chatMessages: currentdate,
    }, function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send(result);
        }
    })
});

app.get('/render', async (req, res) => {
    User.find()
        .then(users => res.send(users))
});
