const {Schema, model} = require('mongoose')
const user = new Schema({
    _id: Number,
    login: String,
    activeSessions: Number,
    timeStudents: Number,
    timePaint: Number,
    timeCurrency: Number,
    timeConverter: Number,
    timeCalculator: Number,
    timeAccount: Number,
    createClick: Number,
    updateClick: Number,
    deleteClick: Number,
    clearClick: Number,
    chatMessages: Date,
    device: String,
    geo: String,
    publicIP: String,
    browser: String
});

module.exports = model('User', user)
