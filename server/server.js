const config = require('../static/config.js');
const helper = require('../helpers/serverHelpers.js');
const bodyParser = require('body-parser');
const {Client} = require('pg');
const express = require('express');
const axios = require('axios');
const server = express();
server.use(bodyParser.json({limit: "50mb"}));
server.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

const client = new Client(config.connectionString);

client.connect();
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: true}));
server.use(helper.cors);
server.listen(config.posrtgresServerPort, function () {
    console.log(`Postgre server started on port ${config.posrtgresServerPort}..`)
});

server.post('/password-forgot', function (req, res) {
    let sql = `SELECT password FROM teacher WHERE  login = $1 AND keyword = $2`;
    client.query(sql, [req.body.login, req.body.keyword], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                res.status(200).json(response.rows);
            } else {
                res.status(501).send("SERVER ERROR");
            }
        }
    });
});

server.post('/get-new-avatar', function (req, res) {
    let sql = `SELECT picture_url FROM teacher WHERE id = $1; `;
    client.query(sql, [req.body.id], (error, response) => {
        if (error) {
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                res.status(200).json(response.rows);
            } else {
                res.status(501).send("SERVER ERROR");
            }
        }
    });
});

server.post('/authorize-teacher', function (req, res) {
    let sql = `SELECT * FROM teacher WHERE login = $1 AND password = $2 ORDER BY id ASC; `;
    client.query(sql, [req.body.login, req.body.password], (error, response) => {
        if (error) {
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                res.status(200).json(response.rows);
            } else {
                res.status(501).send("SERVER ERROR");
            }
        }
    });
});

server.post('/reset-all', function (req, res) {
    for(var i = 0; i < req.body.requestBody.id.length; i++ ){
        let sql = `DELETE FROM course_teacher WHERE id_group = $1 `;
        client.query(sql, [req.body.requestBody.id[i]], (err, response) => {
            if (err) {
                res.status(501).send("SERVER ERROR");
            } else {
                let sql2 = `DELETE FROM account WHERE id_group = $1 `;
                client.query(sql2, [req.body.requestBody.id[i]], (err, response) => {
                    if (err) {
                        res.status(502).send("SERVER ERROR");
                    } else {
                        let sql3 = `DELETE FROM course_group WHERE id = $1 `;
                        client.query(sql3, [req.body.requestBody.id[i]], (err, response) => {
                            if (err) {
                                res.status(503).send("SERVER ERROR");
                            } else {
                                res.status(200).send();
                            }
                        });
                    }
                });
            }
        });
    }
});

server.post('/create-teacher', function (req, res) {
    let newGroupId = 0;
    let newTeacherId = 0;
    let login = req.body.login;
    let sql1 = `INSERT INTO teacher (login,password,email,phone,keyword,picture_url) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;`;
    client.query(sql1, [req.body.login, req.body.password, req.body.email, req.body.phone, req.body.keyword, req.body.picture], (err, response) => {
        if (err) {
            res.status(501).send("SERVER ERROR");
        } else {
            newTeacherId = response.rows[0].id;
            let sql2 = `INSERT INTO course_group (name) VALUES ($1) RETURNING id ;`;
            client.query(sql2, ["untitled"], (err, response) => {
                if (err) {
                    res.status(502).send("SERVER ERROR");
                } else {
                    newGroupId = response.rows[0].id;
                    let sql = `INSERT INTO course_teacher (id_teacher , id_group) VALUES ($1, $2);`;
                    client.query(sql, [newTeacherId, newGroupId], (err, response) => {
                        if (err) {
                            res.status(503).send("SERVER ERROR");
                        } else {
                            axios.post(`http://localhost:5678/`, {
                                _id: newTeacherId,
                                login: login,
                            });
                            res.status(200).send("OK");
                        }
                    });
                }
            });
        }
    });
});

server.get('/teacher-groups', function (req, res) {
    let sql = `SELECT t1.id_group, t2.name FROM course_teacher t1 inner join course_group t2 on t1.id_group = t2.id WHERE id_teacher = $1 ORDER BY t1.id ASC ;`;
    client.query(sql, [req.query.id_teacher], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).json(response.rows);
        }
    });
});

server.get('/all-students', function (req, res) {
    let sql = `SELECT id,fn,ln,age,ht FROM account WHERE id_group = $1 ORDER BY id ASC;`;
    client.query(sql, [req.query.id_group], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).json(response.rows);
        }
    });
});

server.get('/teacher', function (req, res) {
    let sql = `SELECT login,password,email,phone,keyword,about FROM teacher WHERE id = $1 ORDER BY id ASC;`;
    client.query(sql, [req.query.id], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).json(response.rows);
        }
    });
});

server.post('/update-teacher', function (req, res) {
    let sql = `UPDATE teacher SET (login,email,phone) = ($2, $3, $4) WHERE id = $1;`;
    client.query(sql, [req.body.id, req.body.login, req.body.email, req.body.phone], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});


server.post('/update-teacher-info', function (req, res) {
    let sql = `UPDATE teacher SET about = $2 WHERE id = $1;`;
    client.query(sql, [req.body.id, req.body.about], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/save-group-name', function (req, res) {
    let sql = `UPDATE course_group SET name = $2 WHERE id =$1;`;
    client.query(sql, [req.body.id, req.body.name], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/create-student', function (req, res) {
    let sql = `INSERT INTO account (fn,ln,age,ht,id_group) VALUES ($1, $2,$3, $4, $5) ;`;
    client.query(sql, [req.body.student.fn, req.body.student.ln, req.body.student.age, req.body.student.ht, req.body.student.id_group], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/update-student-info', function (req, res) {
    let sql = helper.queryBuilder(req.body);
    client.query(sql, (err) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/delete-student', function (req, res) {
    let sql = `DELETE FROM account WHERE id = $1 `;
    client.query(sql, [req.body.studentId], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/delete-all-students-from-group', function (req, res) {
    let sql = `DELETE FROM account WHERE id_group = $1 `;
    client.query(sql, [req.body.groupId], (err, response) => {
        if (err) {
            res.status(500).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    });
});

server.post('/delete-group', function (req, res) {
    let sql = `DELETE FROM course_teacher WHERE id_group = $1 `;
    client.query(sql, [req.body.groupId], (err, response) => {
        if (err) {
            res.status(501).send("SERVER ERROR");
        } else {
            let sql2 = `DELETE FROM account WHERE id_group = $1 `;
            client.query(sql2, [req.body.id], (err, response) => {
                if (err) {
                    res.status(502).send("SERVER ERROR");
                } else {
                    let sql3 = `DELETE FROM course_group WHERE id = $1 `;
                    client.query(sql3, [req.body.id], (err, response) => {
                        if (err) {
                            res.status(503).send("SERVER ERROR");
                        } else {
                            res.status(200).send("OK");
                        }
                    });
                }
            });
        }
    });
});

server.post('/create-new-group', function (req, res) {
    let newGroupId = 0;
    let sql2 = `INSERT INTO course_group (name) VALUES ($1) RETURNING id ;`;
    client.query(sql2, ["untitled"], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            newGroupId = response.rows[0].id;
            let sql = `INSERT INTO course_teacher (id_teacher , id_group) VALUES ($1, $2);`;
            client.query(sql, [req.body.id, newGroupId], (err, response) => {
                if (err) {
                    res.status(502).send("SERVER ERROR");
                } else {
                    res.status(200).send();
                }
            });
        }
    });
});

server.post('/sendImage', (req, res) => {  //fixme
    client.query(`UPDATE teacher SET picture_url = '${req.body.picture_url}' where id = ${req.body.id}`,
        (err, result) => {
            if (err) {
                console.log(err);
            } else {
                console.log(res.send(result.rows));
            }
        })
});

server.post('/getImage', function (req, res) {
    let sql = `SELECT picture_url FROM teacher WHERE id = $1; `;
    client.query(sql, [req.body.id], (error, response) => {
        if (error) {
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                res.status(200).json(response.rows);
            } else {
                res.status(501).send("SERVER ERROR");
            }
        }
    });
});

////////////WEBSOCKET FOR CHAT /////////////

const webSocket = express();
const webSocketServer = webSocket.listen(config.websocketServerPort, () => {
    console.log(`Websocket running on port ${config.websocketServerPort}...`);
});

const users = [];
const rooms = [{
    id: 1,
    type: "General",
    name: "General chat",
    participants: [],
    messages: [],
}];

const io = require('socket.io')(webSocketServer);
io.sockets.on('connection', socket => {
    socket.on("INIT_USER", teacher => {
        users.push(teacher);
        let userChats = [];
        for (let i = 0; i < rooms.length; i++) {
            if (rooms[i].type === "General") {
                rooms[i].participants.push({...teacher, isRead: true});
                userChats.push(rooms[i]);
            } else {
                for (let j = 0; j < rooms[i].participants.length; j++) {
                    if (teacher.id === rooms[i].participants[j].id) {
                        rooms[i].participants.push({...teacher, isRead: true});
                        userChats.push(rooms[i]);
                        break;
                    }
                }
            }
        }
        users.forEach(item => users.length !== 0 ? io.sockets.sockets[item.socket].emit('SHOW_USERS', users) : null);
        socket.emit("SHOW_CHATS", userChats);
    });

    socket.on('ADD_PRIVATE_CHAT', dataChat => {
        for (let i = 0; i < users.length; i++) {
            if (dataChat.participants[0].id === users[i].id && dataChat.participants[0].socket !== users[i].socket) {
                dataChat.participants.push({...users[i], isRead: true});
            }
            if (dataChat.participants[1].id === users[i].id && dataChat.participants[1].socket !== users[i].socket) {
                dataChat.participants.push({...users[i], isRead: false});
            }
        }
        rooms.push(dataChat);

        for (let k = 0; k < dataChat.participants.length; k++) {
            let userChats = [];
            for (let j = 0; j < rooms.length; j++) {
                for (let y = 0; y < rooms[j].participants.length; y++) {
                    if (dataChat.participants[k].socket === rooms[j].participants[y].socket) {
                        userChats.push(rooms[j]);
                        break;
                    }
                }
            }
            io.sockets.sockets[dataChat.participants[k].socket].emit('SHOW_CHATS', userChats);
            userChats.length = 0;
        }
    });

    socket.on('SEND_MESSAGE', messageData => {
        for (let i = 0; i < rooms.length; i++) {
            if (rooms[i].id === messageData.idRoom) {
                rooms[i].messages.push({login: messageData.login, message: messageData.message});
                for (let j = 0; j < rooms[i].participants.length; j++) {
                    rooms[i].participants[j].isRead = rooms[i].participants[j].id === messageData.idTeacher;
                }
                for (let u = 0; u < rooms[i].participants.length; u++) {
                    let userChats = [];
                    for (let k = 0; k < rooms.length; k++) {
                        for (let y = 0; y < rooms[k].participants.length; y++) {
                            if (rooms[i].participants[u].socket === rooms[k].participants[y].socket) {
                                userChats.push(rooms[k]);
                                break;
                            }
                        }
                    }
                    io.sockets.sockets[rooms[i].participants[u].socket].emit('SHOW_CHATS', userChats);
                }
                break;
            }
        }
    });

    socket.on("READ", dataChat => {
        for (let i = 0; i < rooms.length; i++) {
            if (rooms[i].id === dataChat.idChat) {
                for (let j = 0; j < rooms[i].participants.length; j++) {
                    if (rooms[i].participants[j].id === dataChat.idTeacher) {
                        rooms[i].participants[j].isRead = true;
                    }
                }
                break;
            }
        }

        for (let i = 0; i < users.length; i++) {
            if (users[i].id === dataChat.idTeacher) {
                let userChats = [];
                for (let j = 0; j < rooms.length; j++) {
                    for (let y = 0; y < rooms[j].participants.length; y++) {
                        if (rooms[j].participants[y].socket === users[i].socket) {
                            userChats.push(rooms[j]);
                            break;
                        }
                    }
                }
                io.sockets.sockets[users[i].socket].emit('SHOW_CHATS', userChats);
            }
        }
    });

    socket.on("LEAVE_CHAT", idChat => {
        for (let i = rooms.length - 1; i >= 0; i--) {
            if (rooms[i].id === idChat) {
                let participants = rooms[i].participants;
                rooms.splice(i, 1);
                for (let j = 0; j < participants.length; j++) {
                    let userChats = [];
                    for (let k = 0; k < rooms.length; k++) {
                        for (let y = 0; y < rooms[k].participants.length; y++) {
                            if (participants[j].socket === rooms[k].participants[y].socket) {
                                userChats.push(rooms[k]);
                                break;
                            }
                        }
                    }
                    io.sockets.sockets[participants[j].socket].emit('SHOW_CHATS', userChats);
                }
                break;
            }
        }
    });

    socket.on('GET_DRAWS', teacher => { //must be fixed
        let exist = false;
        for(let i = 0; i < users.length; i++){
            if(users[i].id === teacher.id){
                teacher.lines = users[i].lines;
                socket.emit('SHOW_DRAWED', users[i].lines);
                if(users[i].socket === teacher.socket){
                    exist = true;
                }
            }
        }
        if(!exist) users.push(teacher);
    })

    socket.on('DRAW', data => {
        for(let i = 0; i < users.length; i++){
            if(users[i].id === data.id){
                users[i].lines.push(data.lines);
                if(users[i].socket !== data.socket){
                    io.sockets.sockets[users[i].socket].emit('SHOW_DRAWED', users[i].lines);
                }
            }
        }
    })

    socket.on('CLEAR_CANVAS', data => {
        for(let i = 0; i < users.length; i++){
            if(users[i].id === data.id){
                users[i].lines.length = 0;
                if(users[i].socket !== data.socket){
                    io.sockets.sockets[users[i].socket].emit('SHOW_DRAWED', users[i].lines);
                }
            }
        }
    })

    socket.on('disconnect', () => {
        for (let i = 0; i < users.length; i++) {
            if (users[i].socket === socket.id) {
                users.splice(i, 1);
                break;
            }
        }
        for (let i = rooms.length - 1; i >= 0; i--) {
            if (rooms[i].type === "General") {
                for (let j = 0; j < rooms[i].participants.length; j++) {
                    if (rooms[i].participants[j].socket === socket.id) {
                        rooms[i].participants.splice(j, 1);
                        if (rooms[i].participants.length === 0) {
                            rooms[i].messages.length = 0;
                        }
                        break;
                    }
                }
            } else {
                for (let j = 0; j < rooms[i].participants.length; j++) {
                    if (rooms[i].participants[j].socket === socket.id) {
                        let idUser = rooms[i].participants[j].id;
                        rooms[i].participants.splice(j, 1);
                        let participants = rooms[i].participants;

                        let exist = false;
                        for (let y = 0; y < participants.length; y++) {
                            if (idUser === participants[y].id) {
                                exist = true;
                                break;
                            }
                        }
                        if (!exist) rooms.splice(i, 1);
                        for (let h = 0; h < participants.length; h++) {
                            let userChats = [];
                            for (let k = 0; k < rooms.length; k++) {
                                for (let l = 0; l < rooms[k].participants.length; l++) {
                                    if (participants[h].socket === rooms[k].participants[l].socket) {
                                        userChats.push(rooms[k]);
                                        break;
                                    }
                                }
                            }
                            io.sockets.sockets[participants[h].socket].emit('SHOW_CHATS', userChats);
                            userChats.length = 0;
                        }
                        break;
                    }
                }
            }
        }
        users.forEach(item => io.sockets.sockets[item.socket].emit('SHOW_USERS', users));
    })
});

webSocket.use(
    helper.cors,
    express.json()
);

webSocket.get("/", (request, response) => {
    response.send("Hello, I am WebSocket");
});


