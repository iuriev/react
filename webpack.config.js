const webpack = require ("webpack");
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    entry: ['@babel/polyfill', './src/index.js'],
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].[hash:8].js'
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin({}),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: 'public/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: 'styles.[hash:8].css',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader"
            },
            {
                test: /\.(less|css)$/,
                use: [ MiniCssExtractPlugin.loader, 'css-loader', 'less-loader'],
            },
            {
                test: /\.(svg|png|jpg)$/,
                 loader: 'file-loader',
                options: {
                  name: "[name].[ext]"
                },
            }
        ]
    },
    devtool: 'source-map',
    devServer: {
        port: 8080,
        index: 'index.html',
        hotOnly: true
    }
};
